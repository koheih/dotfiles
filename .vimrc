set encoding=utf-8
set mouse=a
set ttymouse=xterm2
set nobackup
set nocompatible
set t_Co=256
set hlsearch
set incsearch
set wrapscan
set ignorecase
set smartcase
set noswapfile
set number
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set showcmd
set showmatch
set backspace=2
set ruler
set laststatus=2

syntax on
filetype plugin indent on

